import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { joinCommand } from '../src/commands/join';
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { ClientError } from '../src/utils/errors';
import { DiscordGatewayAdapterCreator } from '@discordjs/voice';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'join',
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {
                id: '1'
            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {},
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(joinCommand.name, joinCommand as IVoiceCommandVoiceConnection);

describe('Join command positive', ()=>{
    it('Should execute successfully', async () =>{
        const interactionExtended = {
            ...interaction,
            guild: {
                id: '1',
                voiceStates: {
                    cache: {
                        get: jest.fn(x => undefined)
                    }
                },
                voiceAdapterCreator: (jest.fn(()=> { return { sendPayload: jest.fn() } }) as unknown) as DiscordGatewayAdapterCreator
            }
        }
        await Event.execute(bot,interactionExtended);
        expect(bot.audioPlayers[interactionExtended.guild!.id]).toBeDefined();
    });
});

describe('Join command negative', () => {
    it('Should execute successfully', async () =>{
        const interactionExtended = {
            ...interaction,
            guild: {
                id: '1',
                voiceStates: {
                    cache: {
                        get: jest.fn(x => jest.mock.bind({channelId: 2}))
                    }
                },
                voiceAdapterCreator: (jest.fn(()=> { return { sendPayload: jest.fn() } }) as unknown) as DiscordGatewayAdapterCreator
            }
        }
        await Event.execute(bot,interactionExtended);
        expect(joinCommand.additionalErrors()).toBe(ClientError.ALREADYCONNECTED);
    });
})