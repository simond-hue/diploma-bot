import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection, CommandInteractionOptionResolver } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { playCommand } from '../src/commands/play';
import { ClientError } from '../src/utils/errors';
import { YoutubeSong } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'play',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(playCommand.name, playCommand as IVoiceCommandVoiceConnection);

describe('Play positive', ()=>{
    const song: YoutubeSong = ({
        title: 'asd',
        fromFile: false,
        getInfo: jest.fn(),
        requestedName: 'sample',
        length: 22,
        isLive: false,
        downloadFromInfo: jest.fn(() => new Stream()),
        info: jest.mock,
        formats: jest.mock
    } as unknown) as YoutubeSong
    it('Should add to audioplayer', async () =>{
        audioplayer.addToPlaylist(song);
        expect(audioplayer.playlist.length).toBe(1);
    })
})

describe('Play negative YouTube regex', () => {
    it('Should error with', async () => {
        const interactionExtended = {
            ...interaction,
            options: ({
                getSubcommand: jest.fn(x => 'link'),
                getString: jest.fn(x => 'asdasd.com') 
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, interactionExtended);
        expect((playCommand.subcommand as IVoiceCommandVoiceConnection).commandErrors()).toBe(ClientError.INCORRECTLINK);
    })
})

describe('Play negative file is not audio', () => {
    it('Should error with', async () => {
        const interactionExtended = {
            ...interaction,
            options: ({
                getSubcommand: jest.fn(x => 'file'),
                getAttachment: jest.fn(x => { return { contentType: 'image/png' }})
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, interactionExtended);
        expect((playCommand.subcommand as IVoiceCommandVoiceConnection).commandErrors()).toBe(ClientError.NOTAUDIOTYPE);
    })
})

describe('Play negative file is too big', () => {
    it('Should error with', async () => {
        const interactionExtended = {
            ...interaction,
            options: ({
                getSubcommand: jest.fn(x => 'file'),
                getAttachment: jest.fn(x => { return { contentType: 'audio/mp3', size: 999999999 }})
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, interactionExtended);
        expect((playCommand.subcommand as IVoiceCommandVoiceConnection).commandErrors()).toBe(ClientError.FILETOOLARGE);
    })
})

describe('Play negative YouTube playlist regex', () => {
    it('Should error with', async () => {
        const interactionExtended = {
            ...interaction,
            options: ({
                getSubcommand: jest.fn(x => 'playlist'),
                getString: jest.fn(x => 'https://asdasd.com')
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, interactionExtended);
        expect((playCommand.subcommand as IVoiceCommandVoiceConnection).commandErrors()).toBe(ClientError.INCORRECTLINK);
    })
})