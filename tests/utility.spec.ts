import { formatSecondsToMinutes, getFileLengthInSeconds } from "../src/utils/utility";

describe('Format tests', () => {
    it('Should be 00:00', () =>{
        expect(formatSecondsToMinutes(0)).toBe('00:00')
    })
    it('Should be a normal minute second value', () =>{
        expect(formatSecondsToMinutes(90)).toBe('01:30')
    })
    it('Should be over an hour minute second value', () =>{
        expect(formatSecondsToMinutes(3800)).toBe('63:20')
    })
})

describe('File length in seconds', () => {
    it('Should be 02:10', () =>{
        expect(Math.floor(getFileLengthInSeconds(2087973, 64000))).toBe(130)
    })
})