import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { skipCommand } from '../src/commands/skip';
import { ClientError } from '../src/utils/errors';
import { Song } from '../src/audio/song';
import { Stream } from 'stream';
import { AudioPlayerStatus } from '@discordjs/voice';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'skip',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot
const song: Song = ({
    getInfo: jest.fn(),
    downloadFromInfo: jest.fn(() => new Stream())
} as unknown) as Song

bot.commands.set(skipCommand.name, skipCommand as IVoiceCommandVoiceConnection);

describe('Skip command negative', () =>{
    it('Should error with empty playlist', async () =>{
        await Event.execute(bot, interaction);
        expect(skipCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);
    })
})

describe('Skip command positive', () =>{
    it('Should skip the current song', async () =>{
        audioplayer.addToPlaylist(song);
        audioplayer.emit(AudioPlayerStatus.Playing);
        await Event.execute(bot, interaction);
        audioplayer.emit(AudioPlayerStatus.Idle);
        expect(audioplayer.playlist.length).toBe(0);
    })
})