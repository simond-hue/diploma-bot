import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection, CommandInteractionOptionResolver } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { removeCommand } from '../src/commands/remove';
import { ClientError } from '../src/utils/errors';
import { Song } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'remove',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(removeCommand.name, removeCommand as IVoiceCommandVoiceConnection);

describe('Remove negative bad index', () =>{
    it('Should error bad index', async () =>{
        const extendedInteraction = {
            ...interaction,
            options: ({
                getInteger: jest.fn(x => -1),
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, extendedInteraction);
        expect(removeCommand.commandErrors()).toBe(ClientError.BADINDEX);
    })
})

describe('Remove negative first element', () =>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should error with use skip instead', async () =>{
        audioplayer.playlist = [];
        audioplayer.addToPlaylist(song);
        const extendedInteraction = {
            ...interaction,
            options: ({
                getInteger: jest.fn(x => 1),
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, extendedInteraction);
        expect(removeCommand.commandErrors()).toBe(ClientError.USESKIPINSTEAD);
    })
})

describe('Remove positive', () =>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should error with use skip instead', async () =>{
        audioplayer.playlist = [];
        audioplayer.addToPlaylist(song);
        audioplayer.addToPlaylist(song);
        const extendedInteraction = {
            ...interaction,
            options: ({
                getInteger: jest.fn(x => 2),
            } as unknown) as CommandInteractionOptionResolver,
        }
        await Event.execute(bot, extendedInteraction);
        expect(audioplayer.playlist.length).toBe(1);
    })
})