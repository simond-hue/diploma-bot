import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { queueCommand } from '../src/commands/queue';
import { ClientError } from '../src/utils/errors';
import { Song } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'queue',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock),
    editReply: jest.fn(),
    deferReply: jest.fn(()=> { return { createMessageComponentCollector: jest.fn(()=>{ return { on: jest.fn() } }) } })
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(queueCommand.name, (queueCommand as unknown) as IVoiceCommandVoiceConnection);

describe('Queue command negative', ()=>{
    it('Should error with', async () =>{
        await Event.execute(bot, interaction);
        expect(queueCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);
    })
})

describe('Queue command positive', ()=>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should error with', async () =>{
        audioplayer.addToPlaylist(song);
        await Event.execute(bot, interaction);
        expect(queueCommand.commandErrors()).toBe(ClientError.DEFAULT);
    })
})
