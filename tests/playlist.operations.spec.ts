import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Client, Collection, CommandInteractionOptionResolver } from "discord.js";
import Bot from "../src/client/bot";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import DatabaseManager from '../src/utils/manager';
import { saveCommand } from '../src/commands/saveplaylist';
import { deleteCommand } from '../src/commands/deleteplaylist';
import { loadCommand } from '../src/commands/loadplaylists';
import { listCommand } from '../src/commands/listplaylist';
import { ClientError } from '../src/utils/errors';
import { YoutubeSong } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    guild: {
        id: '1',
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        id: '2',
        voice: {
            channel: {

            }
        },
        user: {
            username: 'zzzzz'
        },
        permissions: {
            has: jest.fn(x => true)
        },
        displayAvatarURL: jest.fn()
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    },
} as unknown) as Bot

const emptyDb = new DatabaseManager('tests/sampledb/emptydb.json');
const oneEntryDb = new DatabaseManager('tests/sampledb/oneentrydb.json');
const maxEntryDb = new DatabaseManager('tests/sampledb/maxentrydb.json');

const emptyBot = ({
    ...bot,
    databaseManager: emptyDb
} as unknown) as Bot

const oneEntryBot = ({
    ...bot,
    databaseManager: oneEntryDb
} as unknown) as Bot

const maxEntryBot = ({
    ...bot,
    databaseManager: maxEntryDb
} as unknown) as Bot

const song: YoutubeSong = ({
    getInfo: jest.fn(),
    downloadFromInfo: jest.fn(() => new Stream()),
} as unknown) as YoutubeSong

bot.commands.set(saveCommand.name, saveCommand as IVoiceCommandVoiceConnection);
bot.commands.set(deleteCommand.name, deleteCommand as ICommand);
bot.commands.set(loadCommand.name, loadCommand as IVoiceCommandVoiceConnection);
bot.commands.set(listCommand.name, listCommand as ICommand);

const saveInteraction = {
    ...interaction,
    commandName: 'save',
    options: ({
        getString: jest.fn(x => 'asd'),
    } as unknown) as CommandInteractionOptionResolver,
}

const deleteInteraction = {
    ...interaction,
    commandName: 'delete',
    options: ({
        getString: jest.fn(x => 'asd'),
    } as unknown) as CommandInteractionOptionResolver,
}

const deleteInteractionSameUserNotAdmin = {
    ...interaction,
    member: {
        id: '2',
        voice: {
            channel: {

            }
        },
        user: {
            username: 'zzzzz'
        },
        permissions: {
            has: jest.fn(x => false)
        }
    },
    commandName: 'delete',
    options: ({
        getString: jest.fn(x => 'asd'),
    } as unknown) as CommandInteractionOptionResolver,
}

const deleteInteractionDifferentUserAdmin = {
    ...deleteInteraction,
    member: {
        id: '3',
        voice: {
            channel: {

            }
        },
        user: {
            username: 'zzzzz3'
        },
        permissions: {
            has: jest.fn(x => true)
        },
    }
}

const deleteInteractionDifferentUserNotAdmin = {
    ...deleteInteraction,
    member: {
        id: '3',
        voice: {
            channel: {

            }
        },
        user: {
            username: 'zzzzz3'
        },
        permissions: {
            has: jest.fn(x => false)
        },
    }
}

const loadInteraction = {
    ...interaction,
    commandName: 'load',
    options: ({
        getString: jest.fn(x => 'asd'),
        getBoolean: jest.fn(x => false)
    } as unknown) as CommandInteractionOptionResolver,
}

const listInteraction = {
    ...interaction,
    commandName: 'list'
}

describe('Save tests', () =>{
    it('Should error with empty playlist', async () => {
        await Event.execute(bot, saveInteraction);
        expect(saveCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);      
    })
    it('Should save playlist', async () => {
        audioplayer.addToPlaylist(song);
        await Event.execute(emptyBot, saveInteraction);
        expect(emptyBot.databaseManager.checkEntry('1', 'asd')).toBe(true);
        emptyBot.databaseManager.deletePlaylist('1', 'asd');
    })
    it('Should error with playlist existing', async () =>{
        await Event.execute(oneEntryBot, saveInteraction);
        expect(saveCommand.commandErrors()).toBe(ClientError.PLAYLISTEXISTS);
        expect(oneEntryBot.databaseManager.checkEntry('1', 'asd')).toBe(true);
    })
    it('Should error with exceeding playlist count', async () =>{
        await Event.execute(maxEntryBot, saveInteraction);
        expect(saveCommand.commandErrors()).toBe(ClientError.TOOMANYPLAYLISTS);
        expect(maxEntryBot.databaseManager.checkEntry('1', 'asd')).toBe(false);
    })
})

describe('Delete tests', () => {
    it('Should error with no such playlist', async () =>{
        await Event.execute(emptyBot, deleteInteraction);
        expect(deleteCommand.errorCheck()).toBe(ClientError.NOSUCHPLAYLIST); 
    })
    it('Should error with no permission to delete', async () =>{
        await Event.execute(oneEntryBot, deleteInteractionDifferentUserNotAdmin);
        expect(deleteCommand.errorCheck()).toBe(ClientError.NOPERMISSIONTODELETE); 
    })
    it('Should delete playlist (different user being admin)', async () =>{
        await Event.execute(oneEntryBot, deleteInteractionDifferentUserAdmin);
        expect(oneEntryBot.databaseManager.checkEntry('1', 'asd')).toBe(false);
        await Event.execute(oneEntryBot, saveInteraction);
    })
    it('Should delete playlist (same user not being admin)', async () =>{
        await Event.execute(oneEntryBot, deleteInteractionSameUserNotAdmin);
        expect(oneEntryBot.databaseManager.checkEntry('1', 'asd')).toBe(false);
        await Event.execute(oneEntryBot, saveInteraction);
    })
    it('Should delete playlist', async () =>{
        await Event.execute(oneEntryBot, deleteInteraction);
        expect(oneEntryBot.databaseManager.checkEntry('1', 'asd')).toBe(false);
        await Event.execute(oneEntryBot, saveInteraction);
    })
})

describe('Load tests', ()=>{
    it('Should error with no such playlist', async () =>{
        await Event.execute(emptyBot, loadInteraction);
        expect(loadCommand.commandErrors()).toBe(ClientError.NOSUCHPLAYLIST);
    })
})

describe('List playlists', ()=>{
    it('Should error with no playlists', async () =>{
        await Event.execute(emptyBot, listInteraction);
        expect(listCommand.errorCheck()).toBe(ClientError.NOPLAYLISTS);
    })
    it('Should run command', async () =>{
        await Event.execute(oneEntryBot, listInteraction);
        expect(listCommand.errorCheck()).toBe(ClientError.DEFAULT);
    })
})