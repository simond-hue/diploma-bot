import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection, PermissionsBitField } from "discord.js";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command"
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { clearCommand } from "../src/commands/clear";
import Bot from "../src/client/bot";
import { ClientError } from '../src/utils/errors';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'clear',
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(clearCommand.name, clearCommand as IVoiceCommandVoiceConnection);

describe('IVoiceCommand negatives', () =>{
    it('Should error with no connect permission', async () =>{
        const interactionExtended ={
            ...interaction,
            appPermissions: {
                has: jest.fn(x => {if(x===PermissionsBitField.Flags.Connect) { return false } else { return true }})
            },
            member: {
                voice: {
                    channel: {
        
                    }
                }
            },
            guild: {
                id: 1,
                voiceStates: {
                    cache: {
                        get: jest.fn(x => jest.mock)
                    }
                }
            }
        }
        await Event.execute(bot, interactionExtended);
        expect(clearCommand.errorCheck()).toBe(ClientError.CONNECTPERMISSION);
    });

    it('Should error with no speak permission', async () =>{
        const interactionExtended ={
            ...interaction,
            appPermissions: {
                has: jest.fn(x => {if(x===PermissionsBitField.Flags.Speak) { return false } else { return true }})
            },
            member: {
                voice: {
                    channel: {
        
                    }
                }
            },
            guild: {
                id: 1,
                voiceStates: {
                    cache: {
                        get: jest.fn(x => jest.mock)
                    }
                }
            }
        }
        await Event.execute(bot, interactionExtended);
        expect(clearCommand.errorCheck()).toBe(ClientError.SPEAKPERMISSION);
    });

    it('Should error with no voice connection', async () =>{
        const interactionExtended ={
            ...interaction,
            appPermissions: {
                has: jest.fn(x => true)
            },
            member: {
                voice: {
                }
            },
            guild: {
                id: 1,
                voiceStates: {
                    cache: {
                        get: jest.fn(x => jest.mock)
                    }
                }
            }
        }
        await Event.execute(bot, interactionExtended);
        expect(clearCommand.errorCheck()).toBe(ClientError.CALLERVOICECHANNEL);
    });
});

describe('IVoiceCommandVoiceConnection negatives', () => {
    it('Should error with no voice connection', async () =>{
        const interactionExtended ={
            ...interaction,
            appPermissions: {
                has: jest.fn(x => true)
            },
            member: {
                voice: {
                    channel: {

                    }
                }
            },
            guild: {
                id: 1,
                voiceStates: {
                    cache: {
                        get: jest.fn(x => undefined)
                    }
                }
            }
        }
        await Event.execute(bot, interactionExtended);
        expect(clearCommand.errorCheck()).toBe(ClientError.NOTCONNECTED);
    });
    it('Should error with no voice connection', async () =>{
        const interactionExtended ={
            ...interaction,
            appPermissions: {
                has: jest.fn(x => true)
            },
            member: {
                voice: {
                    channel: {
                        
                    },
                    channelId: 1
                }
            },
            guild: {
                id: 1,
                voiceStates: {
                    cache: {
                        get: jest.fn(x => jest.mock.bind({channelId: 2}))
                    }
                }
            }
        }
        await Event.execute(bot, interactionExtended);
        expect(clearCommand.errorCheck()).toBe(ClientError.NOTTHESAMECHANNEL);
    });
});