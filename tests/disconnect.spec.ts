import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { disconnectCommand } from '../src/commands/disconnect';
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'disconnect',
    guild: {
        id: '1',
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        '1': audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(disconnectCommand.name, disconnectCommand as IVoiceCommandVoiceConnection);

describe('Disconnect positive', () => {
    it('Should remove', async () =>{
        let id = interaction.guild!.id;
        await Event.execute(bot, interaction);
        expect(bot.audioPlayers[id]).toBe(undefined);
    });
})