import { Event } from '../src/events/interaction';
import Bot from "../src/client/bot";
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { pingCommand } from '../src/commands/ping';
import { ICommand } from '../src/interfaces/command';
const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'ping',
    reply: jest.fn()
} as unknown) as ChatInputCommandInteraction
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    ws: {
        ping: 100
    }
} as unknown) as Bot;
bot.commands.set(pingCommand.name, pingCommand as ICommand);
describe('Ping command', ()=>{
    it('Should call', async () =>{
        await Event.execute(bot, interaction);
        expect(interaction.reply).toHaveBeenCalledWith(`Pong! ${bot.ws.ping}`);
    });
});
