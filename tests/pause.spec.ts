import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import Bot from "../src/client/bot";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import { pauseCommand } from '../src/commands/pause';
import { ClientError } from '../src/utils/errors';
import { Song } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'pause',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(pauseCommand.name, pauseCommand as IVoiceCommandVoiceConnection);

describe('Pause negative empty playlist', ()=>{
    it('Should run', async () =>{
        await Event.execute(bot, interaction);
        expect(pauseCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);
    })
})

describe('Pause negative already paused', ()=>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should run', async () =>{
        audioplayer.addToPlaylist(song);
        await Event.execute(bot, interaction);
        await Event.execute(bot, interaction);
        expect(pauseCommand.commandErrors()).toBe(ClientError.ALREADYPAUSED);
    })
})

describe('Pause positive', ()=>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should run', async () =>{
        audioplayer.addToPlaylist(song);
        await Event.execute(bot, interaction);
        expect(audioplayer.paused).toBe(true);
    })
})
