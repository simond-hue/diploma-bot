import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection, CommandInteractionOptionResolver } from "discord.js";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import { joinCommand } from "../src/commands/join";
import Bot from "../src/client/bot";
import { disconnectCommand } from "../src/commands/disconnect";
import { ClientError } from '../src/utils/errors';
import { YoutubeSong } from '../src/audio/song';
import { Stream } from 'stream';
import { loopCommand } from '../src/commands/loop';
import { AudioPlayerStatus, DiscordGatewayAdapterCreator } from '@discordjs/voice';
import { playlistCommand } from '../src/commands/shuffle';
import { skipCommand } from '../src/commands/skip';
import { removeCommand } from '../src/commands/remove';
import { clearCommand } from '../src/commands/clear';
import { saveCommand } from '../src/commands/saveplaylist';
import { deleteCommand } from '../src/commands/deleteplaylist';
import { loadCommand } from '../src/commands/loadplaylists';
import DatabaseManager from '../src/utils/manager';

const emptyDb = new DatabaseManager('tests/sampledb/emptydb.json');

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    appPermissions: {
        has: jest.fn(x => true)
    },
    guild: {
        id: '1',
        voiceStates: {
            cache: {
                get: jest.fn(x => undefined)
            }
        },
        voiceAdapterCreator: (jest.fn(()=> { return { sendPayload: jest.fn() } }) as unknown) as DiscordGatewayAdapterCreator
    },
    member: {
        voice: {
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: { },
    user: {
        id: 1
    },
    databaseManager: emptyDb
} as unknown) as Bot

bot.commands.set(joinCommand.name, joinCommand as IVoiceCommandVoiceConnection);
bot.commands.set(disconnectCommand.name, disconnectCommand as IVoiceCommandVoiceConnection);
bot.commands.set(loopCommand.name, loopCommand as IVoiceCommandVoiceConnection);
bot.commands.set(playlistCommand.name, playlistCommand as IVoiceCommandVoiceConnection);
bot.commands.set(skipCommand.name, skipCommand as IVoiceCommandVoiceConnection);
bot.commands.set(removeCommand.name, removeCommand as IVoiceCommandVoiceConnection);
bot.commands.set(clearCommand.name, clearCommand as IVoiceCommandVoiceConnection);
bot.commands.set(saveCommand.name, saveCommand as IVoiceCommandVoiceConnection);
bot.commands.set(deleteCommand.name, deleteCommand as ICommand);
bot.commands.set(loadCommand.name, loadCommand as IVoiceCommandVoiceConnection);

const botOnVoice = {
    guild: {
        id: '1',
        voiceStates: {
            cache: {
                get: jest.fn(x => { return { channelId: '1' } })
            }
        },
        voiceAdapterCreator: (jest.fn(()=> { return { sendPayload: jest.fn() } }) as unknown) as DiscordGatewayAdapterCreator
    }
}

const joinInteraction: ChatInputCommandInteraction = ({
    ...interaction,
    commandName: 'join'
} as unknown) as ChatInputCommandInteraction

const interactionMemberVoice: ChatInputCommandInteraction = ({
    ...interaction,
    member: {
        voice: {
            channelId: '1',
            channel: {
                id: '1'
            }
        },
        user: {
            username: 'zzzz'
        },
        permissions: {
            has: jest.fn(x => true)
        },
        displayAvatarURL: jest.fn()
    }
} as unknown) as ChatInputCommandInteraction

const joinInteractionMemberVoice = ({
    ...interactionMemberVoice,
    commandName: 'join'
} as unknown) as ChatInputCommandInteraction

const disconnectInteraction: ChatInputCommandInteraction = ({
    ...interaction,
    commandName: 'disconnect'
} as unknown) as ChatInputCommandInteraction

const disconnectMemberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'disconnect',
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const loopMemberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'loop',
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const skipMemberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'skip',
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const shuffleMemberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'shuffle',
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const removeMemberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'remove',
    options: ({
        getInteger: jest.fn(x => 2),
    } as unknown) as CommandInteractionOptionResolver,
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const clearMemeberVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'clear',
    ...botOnVoice
} as unknown) as ChatInputCommandInteraction

const saveCommandVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'save',
    ...botOnVoice,
    options: ({
        getString: jest.fn(x => 'asd'),
    } as unknown) as CommandInteractionOptionResolver,
} as unknown) as ChatInputCommandInteraction

const loadCommandVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'load',
    ...botOnVoice,
    options: ({
        getString: jest.fn(x => 'asd'),
        getBoolean: jest.fn(x => false)
    } as unknown) as CommandInteractionOptionResolver,
} as unknown) as ChatInputCommandInteraction

const deleteCommandVoiceInteraction: ChatInputCommandInteraction = ({
    ...interactionMemberVoice,
    commandName: 'delete',
    ...botOnVoice,
    options: ({
        getString: jest.fn(x => 'asd'),
    } as unknown) as CommandInteractionOptionResolver
} as unknown) as ChatInputCommandInteraction

const song: YoutubeSong = ({
    title: 'asd',
    fromFile: false,
    getInfo: jest.fn(),
    requestedName: 'sample',
    length: 22,
    isLive: false,
    downloadFromInfo: jest.fn(() => new Stream()),
    info: jest.mock,
    formats: jest.mock
} as unknown) as YoutubeSong

describe('Join and immedietely disconnect', () => {
    it('Join and disconnect tests simmultaniously', async () => {
        await Event.execute(bot, joinInteraction);
        expect(joinCommand.errorCheck()).toBe(ClientError.CALLERVOICECHANNEL);
        await Event.execute(bot, joinInteractionMemberVoice);
        expect(bot.audioPlayers[interaction.guild!.id]).toBeDefined();
        await Event.execute(bot, disconnectInteraction);
        expect(bot.audioPlayers[interaction.guild!.id]).toBeDefined();
        await Event.execute(bot, disconnectMemberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id]).toBeUndefined();
    })
});

describe('Voice command tests', () => {
    it('Should work as expected', async () => {
        await Event.execute(bot, joinInteractionMemberVoice);
        expect(bot.audioPlayers[interaction.guild!.id]).toBeDefined();
        for(let i = 0; i < 10; i++){
            bot.audioPlayers[interaction.guild!.id].addToPlaylist(song);
        }
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Playing);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(10);
        await Event.execute(bot, loopMemberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].looped).toBe(true);
        await Event.execute(bot, skipMemberVoiceInteraction);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Idle);
        expect(bot.audioPlayers[interaction.guild!.id].looped).toBe(false);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(9);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Playing);
        await Event.execute(bot, shuffleMemberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].shuffled).toBe(true);
        await Event.execute(bot, skipMemberVoiceInteraction);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Idle);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(9);
        expect(bot.audioPlayers[interaction.guild!.id].shuffled).toBe(true);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Playing);
        await Event.execute(bot, removeMemberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(8);
        await Event.execute(bot, shuffleMemberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].shuffled).toBe(false);
        await Event.execute(bot, clearMemeberVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(1);
        await Event.execute(bot, skipMemberVoiceInteraction);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Idle);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(1);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Playing);
        await Event.execute(bot, skipMemberVoiceInteraction);
        bot.audioPlayers[interaction.guild!.id].emit(AudioPlayerStatus.Idle);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(0);
    })
});

describe('Database operations continously', () => {
    it('Should work as expected', async () =>{
        bot.audioPlayers[interaction.guild!.id].addToPlaylist(song);
        expect(bot.databaseManager.getEntryCount(interaction.guild!.id)).toBe(0);
        await Event.execute(bot, saveCommandVoiceInteraction);
        expect(bot.databaseManager.getEntryCount(interaction.guild!.id)).toBe(1);
        await Event.execute(bot, loadCommandVoiceInteraction);
        expect(bot.audioPlayers[interaction.guild!.id].playlist.length).toBe(2);
        await Event.execute(bot, deleteCommandVoiceInteraction);
        expect(bot.databaseManager.getEntryCount(interaction.guild!.id)).toBe(0);
    })
});