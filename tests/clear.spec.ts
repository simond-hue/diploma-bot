import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection, PermissionsBitField } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import { Stream } from "stream";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import Bot from "../src/client/bot";
import { Song } from "../src/audio/song";
import { clearCommand } from "../src/commands/clear";
import { ClientError } from '../src/utils/errors';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'clear',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(clearCommand.name, clearCommand as IVoiceCommandVoiceConnection);

describe('Clear command negative', () => {
    it('Should not call', async () => {
        await Event.execute(bot, interaction);
        expect(clearCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);
    });
});

describe('Clear command positive', ()=>{
    const song: Song = ({
        getInfo: jest.fn(),
        downloadFromInfo: jest.fn(() => new Stream())
    } as unknown) as Song
    it('Should call', async () => {
        audioplayer.addToPlaylist(song);
        audioplayer.addToPlaylist(song);    
        await Event.execute(bot, interaction);
        expect(audioplayer.playlist.length).toBe(1);
    });
});
