import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import Bot from "../src/client/bot";
import { nowplaying } from '../src/commands/nowplaying';
import { ClientError } from "../src/utils/errors";
import { FileSong, YoutubeSong } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'nowplaying',
    guild: {
        id: 1,
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        1: audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(nowplaying.name, nowplaying as IVoiceCommandVoiceConnection);

describe('Now playing negative', ()=>{
    it('Should error with', async ()=>{
        await Event.execute(bot, interaction);
        expect(nowplaying.commandErrors()).toBe(ClientError.EMPTYPLAYLIST)
    });
});

describe('Now playing positive youtube song', ()=>{
    const song: YoutubeSong = ({
        title: 'asd',
        fromFile: false,
        getInfo: jest.fn(),
        requestedName: 'sample',
        length: 22,
        isLive: false,
        downloadFromInfo: jest.fn(() => new Stream()),
        info: jest.mock,
        formats: jest.mock
    } as unknown) as YoutubeSong
    it('Should return', async ()=>{
        audioplayer.addToPlaylist(song);
        await Event.execute(bot, interaction);
        expect(nowplaying.errorCheck()).toBe(ClientError.DEFAULT);
        expect(nowplaying.commandErrors()).toBe(ClientError.DEFAULT);
        expect(nowplaying.afterCommandErrorCheck()).toBe(ClientError.DEFAULT)
        expect(audioplayer.getCurrentSong().title).toBe(song.title);
    })
});

describe('Now playing positive file song', ()=>{
    const song: FileSong = ({
        title: 'asd',
        fromFile: true,
        getInfo: jest.fn(),
        requestedName: 'sample',
        length: 22,
        isLive: false,
        downloadFromInfo: jest.fn(() => new Stream()),
    } as unknown) as FileSong
    it('Should return', async ()=>{
        audioplayer.addToPlaylist(song);
        await Event.execute(bot, interaction);
        expect(nowplaying.errorCheck()).toBe(ClientError.DEFAULT);
        expect(nowplaying.commandErrors()).toBe(ClientError.DEFAULT);
        expect(nowplaying.afterCommandErrorCheck()).toBe(ClientError.DEFAULT)
        expect(audioplayer.getCurrentSong().title).toBe(song.title);
    })
});