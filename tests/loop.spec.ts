import { Event } from '../src/events/interaction';
import { ChatInputCommandInteraction, Collection } from "discord.js";
import { AutomatedAudioPlayer } from "../src/audio/audioplayer";
import Bot from "../src/client/bot";
import { ICommand, IVoiceCommandVoiceConnection } from "../src/interfaces/command";
import { loopCommand } from '../src/commands/loop'
import { ClientError } from '../src/utils/errors';
import { Song } from '../src/audio/song';
import { Stream } from 'stream';

const interaction: ChatInputCommandInteraction = ({
    isCommand: jest.fn(() => true),
    isChatInputCommand: jest.fn(() => true),
    isButton: jest.fn().bind(() => false),
    isAutocomplete: jest.fn().bind(() => false),
    commandName: 'loop',
    guild: {
        id: '1',
        voiceStates: {
            cache: {
                get: jest.fn(x => jest.mock)
            }
        }
    },
    appPermissions: {
        has: jest.fn(x => true)
    },
    member: {
        voice: {
            channel: {

            }
        }
    },
    reply: jest.fn(x => jest.mock)
} as unknown) as ChatInputCommandInteraction
const audioplayer = new AutomatedAudioPlayer();
const bot: Bot = ({
    commands: new Collection<string, ICommand>(),
    audioPlayers: {
        '1': audioplayer
    },
    user: {
        id: 1
    }
} as unknown) as Bot

bot.commands.set(loopCommand.name, loopCommand as IVoiceCommandVoiceConnection);

describe('Loop negative', () => {
    it('Should return error', async () =>{
        await Event.execute(bot,interaction);
        expect(loopCommand.commandErrors()).toBe(ClientError.EMPTYPLAYLIST);
    })
})

describe('Loop positive', () => {
    it('Should return error', async () =>{
        const song: Song = ({
            getInfo: jest.fn(),
            downloadFromInfo: jest.fn(() => new Stream())
        } as unknown) as Song
        audioplayer.addToPlaylist(song);
        await Event.execute(bot,interaction);
        expect(audioplayer.looped).toBe(true);
        expect(loopCommand.successMessage).toBe('Looping the current song.');
        await Event.execute(bot,interaction);
        expect(audioplayer.looped).toBe(false);
        expect(loopCommand.successMessage).toBe('Loop has been turned off.');
    })
})