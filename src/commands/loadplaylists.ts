import { Attachment, GuildMember } from "discord.js";
import { FileSong, YoutubeSong } from "../audio/song";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";

class LoadCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'load';
        this.description = 'Loads the requested saved playlist into the player\'s playlist.';
        this.successMessage = 'Loading was successful.';
        this.options = [
            {
                type: 3,
                name: 'name',
                description: 'The name of the playlist.',
                required: true,
                autocomplete: true,
            },
            {
                type: 5,
                name: 'overwrite',
                description: 'Whether the requested playlist should overwrite the current playlist completely.'
            }
        ]
    }
    
    async run(): Promise<any> {
        const playlistName = this.interaction.options.getString('name');
        const playlistData = this.client.databaseManager.getPlaylistsOn(this.guild.id)[playlistName];
        const audioPlayer = this.client.audioPlayers[this.guild.id];
        const requestedBy = this.interaction.member as GuildMember;
        if(this.interaction.options.getBoolean('overwrite')){
            audioPlayer.clearPlaylist();
            audioPlayer.skip();
        }
        playlistData.playlist.forEach((data: any) => {
            audioPlayer.addToPlaylist(data.fromFile ? new FileSong(data.attachment as Attachment, requestedBy) : new YoutubeSong(data.link, requestedBy))
        });
    }

    async autocomplete(client: any, interaction: any): Promise<void> {
        const focusedValue = interaction.options.getFocused();
        const playlistData = client.databaseManager.getPlaylistsOn(interaction.guild.id);
		const choices = Object.keys(playlistData);
		const filtered = choices.filter(choice => choice.startsWith(focusedValue));
		return await interaction.respond(
			filtered.map(choice => ({ name: choice, value: choice })),
		);
    }

    commandErrors(): ClientError {
        const playlistName = this.interaction.options.getString('name');
        if(!this.client.databaseManager.checkEntry(this.guild.id, playlistName)){
            return ClientError.NOSUCHPLAYLIST;
        }
        return ClientError.DEFAULT;
    }
}

export const loadCommand = new LoadCommand().toJSON();