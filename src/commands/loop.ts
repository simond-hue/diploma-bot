import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";

class LoopCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'loop';
        this.description = 'Loops the currently playing audio.';
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        let looped = audioPlayer.toggleLoop();
        this.successMessage = looped ? 'Looping the current song.' : 'Loop has been turned off.'
    }

    commandErrors(): ClientError {
        if(this.client.audioPlayers[this.guild.id].isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        return ClientError.DEFAULT;
    }
}

export const loopCommand = new LoopCommand().toJSON();