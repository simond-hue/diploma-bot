import { FileSong, Song, YoutubeSong } from "../audio/song";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";
import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ComponentType, GuildMember } from "discord.js";
import YouTubeAPI from "../utils/api/youtube";
import { ResultsEmbedBuilder, SuccessEmbedBuilder } from "../utils/embeds";

class PlayCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'play';
        this.description = 'The bot plays the given audio. Can be a YouTube link or a file.';
        this.options = [
            {
                type: 1,
                name: 'link',
                description: 'YouTube link to play audio from.',
                options: [
                    {
                        type: 3,
                        name: 'link',
                        description: 'The link to play.',
                        required: true
                    }
                ]
            },
            {
                type: 1,
                name: 'file',
                description: 'Attachment to play audio from.',
                options: [
                    {
                        type: 11,
                        name: 'file',
                        description: 'The file to play.',
                        required: true
                    }
                ]
            },
            {
                type: 1,
                name: 'playlist',
                description: 'Load a YouTube playlist into the player\'s playlist.',
                options: [
                    {
                        type: 3,
                        name: 'link',
                        description: 'The link to the YouTube playlist.',
                        required: true
                    }
                ]
            },
            {
                type: 1,
                name: 'first',
                description: 'Plays the first search option from YouTube by text.',
                options: [
                    {
                        type: 3,
                        name: 'text',
                        description: 'The text to search for.',
                        required: true
                    }
                ]
            },
            {
                type: 1,
                name: 'search',
                description: 'Searches the text provided on YouTube.',
                options: [
                    {
                        type: 3,
                        name: 'text',
                        description: 'The text to search for.',
                        required: true
                    }
                ]
            }
        ]
    }

    async run(): Promise<any> {
        if(this.interaction.options.getSubcommand() === 'link'){
            this.subcommand = new PlayYouTubeLinkSubcommand(); 
            this.subcommand.execute(this.client, this.interaction);
        }
        if(this.interaction.options.getSubcommand() === 'file'){
            this.subcommand = new PlayFileSubcommand();
            this.subcommand.execute(this.client, this.interaction);
        }
        if(this.interaction.options.getSubcommand() === 'playlist'){
            this.subcommand = new PlaylistSubcommand();
            this.subcommand.execute(this.client, this.interaction);
        }
        if(this.interaction.options.getSubcommand() === 'first'){
            this.subcommand = new PlayFirstSubcommand();
            this.subcommand.execute(this.client, this.interaction);
        }
        if(this.interaction.options.getSubcommand() === 'search'){
            this.subcommand = new PlaySearchSubcommand();
            this.subcommand.execute(this.client, this.interaction);
        }
    };

    commandErrors(): ClientError {
        return ClientError.DEFAULT;
    }

    toJSON(){
        return {
            ...super.toJSON(),
            subcommand: this.subcommand,
        }
    }
}

class PlayYouTubeLinkSubcommand extends IVoiceCommandVoiceConnection{
    readonly youtubeLinkRegex: RegExp = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/
    constructor(){
        super();
        this.name = 'PlayYouTubeLink';
        this.description = 'The bot plays the given audio. Can be a YouTube link or a file.';
    }

    async run(): Promise<any> {
        let player = this.client.audioPlayers[this.guild.id]
        const link = this.interaction.options.getString('link');
        const requestedBy = this.interaction.member;
        let song = new YoutubeSong(link, requestedBy as GuildMember);
        await (song as YoutubeSong).getInfo();
        if(song.error) {
            return;
        }
        player.addToPlaylist(song);
        this.successMessage = `${song.title} has been added to the playlist successfully!`;
    }

    commandErrors(): ClientError {
        const link = this.interaction.options.getString('link');
        if(!this.youtubeLinkRegex.test(link)){
            return ClientError.INCORRECTLINK;
        }
        return ClientError.DEFAULT;
    }

    toJSON(){
        return {
            ...super.toJSON(),
            youtubeLinkRegex: this.youtubeLinkRegex,
        }
    }
}

class PlayFileSubcommand extends IVoiceCommandVoiceConnection{
    protected song: FileSong;
    readonly maxAttachmentSize = 8000000;
    constructor(){
        super();
        this.name = 'PlayFile';
        this.description = 'The bot plays the given audio. Can be a YouTube link or a file.';
    }

    async run(): Promise<any> {
        let player = this.client.audioPlayers[this.guild.id]
        const file = this.interaction.options.getAttachment('file');
        const requestedBy = this.interaction.member as GuildMember;
        this.song = new FileSong(file, requestedBy);
        if(this.song.error) {
            return;
        }
        player.addToPlaylist(this.song);
        this.successMessage = `${this.song.title} has been added to the playlist successfully!`;
    }

    commandErrors(): ClientError {
        const file = this.interaction.options.getAttachment('file');
        if(!file.contentType.startsWith('audio/')){
            return ClientError.NOTAUDIOTYPE;
        }
        if(file.size > this.maxAttachmentSize){
            return ClientError.FILETOOLARGE;
        }
        return ClientError.DEFAULT;
    }
}

class PlaylistSubcommand extends IVoiceCommandVoiceConnection{
    readonly youtubePlaylistLinkRegex: RegExp = /^https?:\/\/(www.youtube.com|youtube.com)\/playlist(.*)$/
    constructor(){
        super();
        this.name = 'PlayYoutubePlaylist';
        this.description = 'The bot loads the given playlist.';
        this.successMessage = 'Requested playlist has been added to the playlist successfully!';
    }

    async run(): Promise<any> {
        let player = this.client.audioPlayers[this.guild.id]
        const link = this.interaction.options.getString('link');
        const playlistID = link.split('?')[1].substring(5);
        const requestedBy = this.interaction.member;
        await this.interaction.deferReply();
        let playlist = await YouTubeAPI.getPlaylist(playlistID);
        playlist.forEach((item: string) =>{
            player.addToPlaylist(new YoutubeSong(item, requestedBy as GuildMember));
        });
    }

    commandErrors(): ClientError {
        const link = this.interaction.options.getString('link');
        if(!this.youtubePlaylistLinkRegex.test(link)){
            return ClientError.INCORRECTLINK;
        }
        return ClientError.DEFAULT;
    }

    toJSON(){
        return {
            ...super.toJSON(),
            youtubeLinkRegex: this.youtubePlaylistLinkRegex,
        }
    }
}

class PlayFirstSubcommand extends IVoiceCommandVoiceConnection{
    private link: string;
    constructor(){
        super();
        this.name = 'PlayFirstSubcommand';
        this.description = 'The bot plays the first search option based on the text given.';
    }

    async run(): Promise<any> {
        let player = this.client.audioPlayers[this.guild.id]
        const requestedBy = this.interaction.member;
        await this.interaction.deferReply();
        let song = new YoutubeSong(this.link, requestedBy as GuildMember);
        await (song as YoutubeSong).getInfo();
        if(song.error) {
            return;
        }
        player.addToPlaylist(song);
        this.successMessage = `${song.title} has been added to the playlist successfully!`;
    }

    async commandErrors(): Promise<ClientError> {
        let foundElements = await YouTubeAPI.search(this.interaction.options.getString('text'), 1);
        if(foundElements.length < 1){
            return ClientError.NOVIDEOFOUND;
        }
        else{
            this.link = foundElements[0];
        }
        return ClientError.DEFAULT;
    }
}

class PlaySearchSubcommand extends IVoiceCommandVoiceConnection{
    private foundElements: string[] = [];
    private readonly searchTimeoutMS = 20000;
    constructor(){
        super();
        this.name = 'PlaySearchSubcommand';
        this.description = 'The bot searches on YouTube for the text provided.';
    }
    async run(): Promise<any> {
        const requestedBy = this.interaction.member;
        let songs: YoutubeSong[] = [];
        let message = await this.interaction.deferReply();
        this.foundElements.forEach(async(item: string) => {
            songs.push(new YoutubeSong(item, requestedBy as GuildMember));
        });
        await Promise.all(songs.map(async(song: YoutubeSong) => { await song.getInfo(); }));
        let collector = message.createMessageComponentCollector({componentType: ComponentType.Button, time: this.searchTimeoutMS, max: 1 });
        collector.on('collect', (buttoninteraction) =>{
            if(buttoninteraction.user.id !== this.caller.id) return;
            this.interaction.editReply({
                embeds: [new ResultsEmbedBuilder(songs)],
                components: []
            })
            let player = this.client.audioPlayers[this.guild.id];
            let song = songs[+buttoninteraction.customId]
            player.addToPlaylist(song);
            this.successMessage = `${song.title} has been added to the playlist succesfully.`;
            buttoninteraction.reply({ embeds: [new SuccessEmbedBuilder(this.successMessage)] });
        })
        collector.on('end', () =>{
            this.interaction.editReply({
                embeds: [new ResultsEmbedBuilder(songs)],
                components: []
            })
        });
        await this.interaction.editReply({
            embeds: [new ResultsEmbedBuilder(songs)],
            components: [
                new ActionRowBuilder<ButtonBuilder>().setComponents(
                    songs.map((item: YoutubeSong) => {
                        let index = songs.indexOf(item);
                        return new ButtonBuilder().setCustomId(`${index}`).setLabel(`${index+1}`).setStyle(ButtonStyle.Primary);
                    })
                )
            ]
        })

    }
    async commandErrors(): Promise<ClientError> {
        this.foundElements = await YouTubeAPI.search(this.interaction.options.getString('text'), 5);
        if(this.foundElements.length < 1){
            return ClientError.NOVIDEOFOUND;
        }
        return ClientError.DEFAULT;
    }
}

export const playCommand = new PlayCommand().toJSON();