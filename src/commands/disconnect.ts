import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { getVoiceConnection } from "@discordjs/voice";

class DisconnectCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'disconnect';
        this.description = 'The bot disconnect from the voice channel.';
        this.successMessage = 'Successfully disconnected from the voice channel.'
    }
    
    async run(): Promise<any> {
        getVoiceConnection(this.guild.id)?.disconnect();
        delete this.client.audioPlayers[this.guild.id];
    }

    commandErrors(): ClientError {
        return ClientError.DEFAULT;
    }
}

export const disconnectCommand = new DisconnectCommand().toJSON();