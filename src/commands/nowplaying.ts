import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { NowPlayingEmbedBuilder } from "../utils/embeds";
import { InteractionResponse } from "discord.js";

class NowPlayingCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'nowplaying';
        this.description = 'Gets the information of the currently playing song.';
    }
    
    async run(): Promise<any> { }

    commandErrors(): ClientError {
        if(this.client.audioPlayers[this.guild.id].isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        return ClientError.DEFAULT;
    }

    async sendSuccessMessage(): Promise<InteractionResponse> {
        let audioPlayer = this.client.audioPlayers[this.guild.id]
        let currentSong = audioPlayer.getCurrentSong();
        return await this.interaction.reply({
            files: currentSong.fromFile ? [{
                attachment:  'media/file.png' 
            }] : [],
            embeds: [new NowPlayingEmbedBuilder(currentSong, audioPlayer.time)]
        })
    }
}

export const nowplaying = new NowPlayingCommand().toJSON();