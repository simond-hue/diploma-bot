import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { PauseEmbedBuilder } from "../utils/embeds";
import { InteractionResponse } from "discord.js";

class PauseCommand extends IVoiceCommandVoiceConnection{
    successful: boolean;
    constructor(){
        super();
        this.name = 'pause';
        this.description = 'Pauses the audio stream.';
        this.successMessage = 'Successfully paused.'
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        this.successful = audioPlayer.pause();
    }

    afterCommandErrorCheck(): ClientError {
        if(!this.successful){ 
            return ClientError.PAUSEERROR;
        }
        return ClientError.DEFAULT;
    }

    commandErrors(): ClientError {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        if(audioPlayer.isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        if(audioPlayer.paused){
            return ClientError.ALREADYPAUSED;
        }
        return ClientError.DEFAULT
    }

    async sendSuccessMessage(): Promise<InteractionResponse> {
        return await this.interaction.reply({
            embeds: [new PauseEmbedBuilder(this.successMessage)]
        })
    }
}

export const pauseCommand = new PauseCommand().toJSON();