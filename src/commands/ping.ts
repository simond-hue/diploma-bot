import { ICommand } from "../interfaces/command";

class PingCommand extends ICommand{
    constructor(){
        super();
        this.name = 'ping';
        this.description = 'Shows the delay between the bot and Discord';
    }
    async run(): Promise<any> {
        await this.interaction.reply(`Pong! ${this.client.ws.ping}`);
    };
}

export const pingCommand = new PingCommand().toJSON();