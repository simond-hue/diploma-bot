import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { UnpauseEmbedBuilder } from "../utils/embeds";
import { InteractionResponse } from "discord.js";

class UnpauseCommand extends IVoiceCommandVoiceConnection{
    successful: boolean;
    constructor(){
        super();
        this.name = 'resume';
        this.description = 'Resumes the audio stream.';
        this.successMessage = 'Successfully unpaused.'
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        this.successful = audioPlayer.unpause();
    }

    afterCommandErrorCheck(): ClientError {
        if(!this.successful){ 
            return ClientError.UNPAUSEERROR;
        }
        return ClientError.DEFAULT;
    }

    commandErrors(): ClientError {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        if(audioPlayer.isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        if(!audioPlayer.paused){
            return ClientError.ALREADYUNPAUSED;
        }
        return ClientError.DEFAULT
    }

    async sendSuccessMessage(): Promise<InteractionResponse>{
        return await this.interaction.reply({
            embeds: [new UnpauseEmbedBuilder(this.successMessage)]
        })
    }
}

export const unpauseCommand = new UnpauseCommand().toJSON();