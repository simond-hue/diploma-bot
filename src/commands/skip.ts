import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";

class SkipCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'skip';
        this.description = 'Skips the currently playing song.';
        this.successMessage = 'Skipped.'
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        audioPlayer.skip();
    }

    afterCommandErrorCheck(): ClientError {
        return ClientError.DEFAULT;
    }

    commandErrors(): ClientError {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        if(audioPlayer.isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        return ClientError.DEFAULT
    }
}

export const skipCommand = new SkipCommand().toJSON();