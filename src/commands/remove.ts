import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";

class RemoveCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'remove';
        this.description = 'Removes the requested song from the playlist.';
        this.options = [
            {
                type: 4,
                name: 'index',
                description: 'The requested index to remove (starting from 1).',
                required: true
            }
        ]
        this.successMessage = 'Remove was successful.'
    }
    
    async run(): Promise<any> {
        let index = this.interaction.options.getInteger('index');
        const player = this.client.audioPlayers[this.guild.id];
        player.remove(index-1);
    }

    commandErrors(): ClientError {
        const player = this.client.audioPlayers[this.guild.id];
        let index = this.interaction.options.getInteger('index');
        if(index > player.playlist.length || index < 1){
            return ClientError.BADINDEX;
        }
        if(index === 1){
            return ClientError.USESKIPINSTEAD;
        }
        return ClientError.DEFAULT
    }
}

export const removeCommand = new RemoveCommand().toJSON();