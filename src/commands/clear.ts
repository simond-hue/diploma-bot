import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";

class ClearCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'clear';
        this.description = 'Clears the current playlist.';
        this.successMessage = 'The playlist has been successfully cleared.'
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        audioPlayer.clearPlaylist();
    }

    commandErrors(): ClientError {
        if(this.client.audioPlayers[this.guild.id].isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        return ClientError.DEFAULT;
    }
}

export const clearCommand = new ClearCommand().toJSON();