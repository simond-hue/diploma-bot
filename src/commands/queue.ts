import { ActionRowBuilder, ButtonBuilder, ButtonStyle, ComponentType } from "discord.js";
import { Song, YoutubeSong } from "../audio/song";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";
import { QueueEmbedBuilder } from "../utils/embeds";

class QueueCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'queue';
        this.description = 'Shows the current playlist.';
    }
    
    async run(): Promise<any> {
        let message = await this.interaction.deferReply();
        const audioPlayer = this.client.audioPlayers[this.guild.id];
        audioPlayer.setPageToDefault();
        let collector = message.createMessageComponentCollector({componentType: ComponentType.Button, time: 20000 });
        collector.on('collect', (buttoninteraction) =>{
            if(buttoninteraction.user.id !== this.caller.id) return;
            if(buttoninteraction.customId === 'increment'){
                audioPlayer.addToPage();
            }
            if(buttoninteraction.customId === 'decrement'){
                audioPlayer.subtractFromPage();
            }
            this.editEmbed();
        })
        collector.on('end', () =>{
            this.editEmbed(false);
        });
        await this.editEmbed();
    }

    async editEmbed(addComponents: boolean = true){
        const audioPlayer = this.client.audioPlayers[this.guild.id];
        let songs = await this.getSongs();
        let embed = new QueueEmbedBuilder(songs, audioPlayer.getPage(), audioPlayer.getMaxPage())
        await this.interaction.editReply({
            embeds: [embed],
            components: addComponents ? [
                new ActionRowBuilder<ButtonBuilder>().setComponents(
                    new ButtonBuilder().setCustomId('decrement').setLabel('<').setDisabled(audioPlayer.isPageFirstPage()).setStyle(ButtonStyle.Primary),
                    new ButtonBuilder().setCustomId('increment').setLabel('>').setDisabled(audioPlayer.isPageMaxPage()).setStyle(ButtonStyle.Primary)
                )
            ] : []
        })
    }

    async getSongs(){
        const audioPlayer = this.client.audioPlayers[this.guild.id];
        let playlistElements = audioPlayer.getPageOfSongs();
        await Promise.all(playlistElements.map(async(element: any) => { if(!element.song.fromFile) await (element.song as YoutubeSong).getInfo(); }));
        return playlistElements;
    }

    commandErrors(): ClientError {
        const audioPlayer = this.client.audioPlayers[this.guild.id];
        if(audioPlayer.isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        return ClientError.DEFAULT;
    }

    toJSON(){
        return {
            ...super.toJSON(),
            getSongs: this.getSongs,
            editEmbed: this.editEmbed
        };
    }
}

export const queueCommand = new QueueCommand().toJSON();