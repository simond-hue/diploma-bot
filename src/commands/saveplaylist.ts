import DatabaseManager from "../utils/manager";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";
import { ClientError } from "../utils/errors";

class SaveCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'save';
        this.options = [
            {
                type: 3,
                name: 'name',
                description: 'The name of the playlist which should be saved as. Must be unique on the server.',
                required: true
            }
        ]
        this.description = 'Saves the current playlist into the bot\'s database.';
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        let name = this.interaction.options.getString('name');
        this.client.databaseManager.savePlaylist(name, this.guild.id, { playlist: audioPlayer.playlist, userID: this.caller.id, creatorName: this.caller.user.username});
        this.successMessage = `${name} playlist has been saved successfully.`
    }

    commandErrors(): ClientError {
        if(this.client.audioPlayers[this.guild.id].isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        let name = this.interaction.options.getString('name')
        let dbmanager = this.client.databaseManager;
        if(dbmanager.checkEntry(this.guild.id, name)){
            return ClientError.PLAYLISTEXISTS;
        }
        if(dbmanager.getEntryCount(this.guild.id) >= DatabaseManager.maxEntries){
            return ClientError.TOOMANYPLAYLISTS;
        }
        return ClientError.DEFAULT;
    }
}

export const saveCommand = new SaveCommand().toJSON();