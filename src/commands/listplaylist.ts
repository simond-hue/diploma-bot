import { ICommand, IVoiceCommandVoiceConnection } from "../interfaces/command";
import { PlaylistEmbedBuilder } from "../utils/embeds";
import { ClientError } from "../utils/errors";

class ListCommand extends ICommand{
    constructor(){
        super();
        this.name = 'list';
        this.description = 'Lists the playlists on this server.';
    }
    
    async run(): Promise<any> {
        const dbmanager = this.client.databaseManager;
        const playlists = dbmanager.getPlaylistsOn(this.guild.id);
        return await this.interaction.reply({ embeds: [new PlaylistEmbedBuilder(playlists)] })
    }

    errorCheck(): ClientError {
        if(!this.client.databaseManager.checkServer(this.guild.id) || this.client.databaseManager.getEntryCount(this.guild.id) === 0){
            return ClientError.NOPLAYLISTS;
        }
        return ClientError.DEFAULT;
    }
}

export const listCommand = new ListCommand().toJSON();