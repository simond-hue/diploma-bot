import DatabaseManager from "../utils/manager";
import { ICommand } from "../interfaces/command";
import { ClientError } from "../utils/errors";
import { PermissionsBitField } from "discord.js";

class DeleteCommand extends ICommand{
    constructor(){
        super();
        this.name = 'delete';
        this.description = 'Deletes the requested saved playlist from the database.';
        this.successMessage = 'Deleting was successful.';
        this.options = [
            {
                type: 3,
                name: 'name',
                description: 'The name of the playlist.',
                required: true,
                autocomplete: true,
            }
        ]
    }
    
    async run(): Promise<any> {
        const playlistName = this.interaction.options.getString('name');
        this.client.databaseManager.deletePlaylist(this.guild.id, playlistName);
    }

    async autocomplete(client: any, interaction: any): Promise<void> {
        const focusedValue = interaction.options.getFocused();
        const playlistData = client.databaseManager.getPlaylistsOn(interaction.guild.id);
		const choices = Object.keys(playlistData);
		const filtered = choices.filter(choice => choice.startsWith(focusedValue));
		return await interaction.respond(
			filtered.map(choice => ({ name: choice, value: choice })),
		);
    }

    errorCheck(): ClientError {
        const playlistName = this.interaction.options.getString('name');
        const playlistData = this.client.databaseManager.getPlaylistsOn(this.guild.id)[playlistName];
        if(!this.client.databaseManager.checkEntry(this.guild.id, playlistName)){
            return ClientError.NOSUCHPLAYLIST;
        }
        if(this.caller.id !== playlistData.createdBy && !this.caller.permissions.has(PermissionsBitField.Flags.Administrator)){
            return ClientError.NOPERMISSIONTODELETE;
        }
        return ClientError.DEFAULT;
    }
}

export const deleteCommand = new DeleteCommand().toJSON();