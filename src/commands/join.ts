import { PermissionsBitField } from "discord.js";
import { IVoiceCommand } from "../interfaces/command";
import { joinVoiceChannel } from "@discordjs/voice";
import { ClientError } from "../utils/errors";
import { AutomatedAudioPlayer } from "../audio/audioplayer";

class JoinCommand extends IVoiceCommand{
    constructor(){
        super();
        this.name = 'join';
        this.description = 'The bot joins to the callers voice channel.';
        this.successMessage = 'Successfully connected to voice channel.'
    }

    additionalErrors(): ClientError {
        if(this.clientVoiceStateOnGuild() !== undefined && this.clientVoiceStateOnGuild().channelId !== null){
            return ClientError.ALREADYCONNECTED;
        }
        return ClientError.DEFAULT;
    }

    async run(): Promise<any> {
        const voiceConnection = joinVoiceChannel({
            channelId: this.caller.voice.channel.id,
            guildId: this.guild.id,
            adapterCreator: this.guild.voiceAdapterCreator
        });
        const player = new AutomatedAudioPlayer();
        this.client.audioPlayers[this.guild.id] = player;
        voiceConnection.subscribe(player);
    };
}

export const joinCommand = new JoinCommand().toJSON();