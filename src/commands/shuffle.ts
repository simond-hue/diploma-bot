import { ClientError } from "../utils/errors";
import { IVoiceCommandVoiceConnection } from "../interfaces/command";

class ShuffleCommand extends IVoiceCommandVoiceConnection{
    constructor(){
        super();
        this.name = 'shuffle';
        this.description = 'Shuffles the current playlist.';
    }
    
    async run(): Promise<any> {
        let audioPlayer = this.client.audioPlayers[this.guild.id];
        let shuffled = audioPlayer.toggleShuffle();
        this.successMessage = shuffled ? 'Shuffle has been turned on.' : 'Shuffle has been turned off.'
    }

    commandErrors(): ClientError {
        if(this.client.audioPlayers[this.guild.id].isPlaylistEmpty()){
            return ClientError.EMPTYPLAYLIST;
        }
        if(this.client.audioPlayers[this.guild.id].playlist.length === 1){
            return ClientError.USELOOPINSTEAD;
        }
        return ClientError.DEFAULT;
    }
}

export const playlistCommand = new ShuffleCommand().toJSON();