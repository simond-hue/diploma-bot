import { commandErrorEmbed } from '../utils/embeds';
import { Events, Interaction, EmbedBuilder, ButtonInteraction, ChatInputCommandInteraction } from 'discord.js';
import { isAsyncFunction } from 'util/types';
import { IEvent } from '../interfaces/event';
import Bot from '@/client/bot';

export const Event: IEvent = {
    name: Events.InteractionCreate,
    type: 'on',
    execute: async (client: Bot, interaction: Interaction) => {
        if(interaction.isButton()) return interaction.deferUpdate();
        if(!((interaction.isCommand() && interaction.isChatInputCommand()) || interaction.isAutocomplete())){
            return;
        }
        const command = client.commands.get(interaction.commandName);
        if (!command) return;

        if(interaction.isAutocomplete()){
            try {
                await command.autocomplete(client, interaction);
            } catch (error) {
                console.error(error);
            }
        }
        if(interaction.isCommand() && interaction.isChatInputCommand()){
            const errorHandler = (err: Error) => {
                console.error(err);
    
                interaction.reply({
                    embeds: [commandErrorEmbed]
                });
            };
            if (isAsyncFunction(command.execute)) {
                command.execute(client, interaction).catch(errorHandler);
            } else {
                try {
                    command.execute(client, interaction);
                } catch(err) {
                    errorHandler(err);
                };
            };
        }
        
    }
};