import { ClientEvents } from 'discord.js';
import Bot from '../client/bot';

export interface IEvent {
    name: keyof ClientEvents;
    type: 'on' | 'once';
    execute: (client: Bot, ...args: any) => Promise<any> | any;
};