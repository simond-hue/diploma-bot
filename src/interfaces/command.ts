import { ErrorEmbedBuilder, SuccessEmbedBuilder } from '../utils/embeds';
import { ChatInputCommandInteraction, Guild, GuildMember, InteractionResponse, Message, PermissionsBitField } from 'discord.js';
import Bot from '../client/bot';
import { ClientError } from '../utils/errors';

export interface IOption {
    type: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    name: string;
    description: string;
    required?: boolean;
    options?: IOption[];
    autocomplete?: boolean;
};

export abstract class ICommand {
    name: string;
    description: string;
    options?: IOption[];
    successMessage: string;
    subcommand: ICommand;

    protected permissions: PermissionsBitField = new PermissionsBitField();
    protected caller: GuildMember;
    protected guild: Guild;
    protected client: Bot;
    protected interaction: ChatInputCommandInteraction;

    constructor(){ }

    checkRequirements(): ClientError | Promise<ClientError> {
        return this.errorCheck();
    }
    errorCheck(): ClientError | Promise<ClientError> { return ClientError.DEFAULT; }
    afterCommandErrorCheck(): ClientError { return ClientError.DEFAULT; }
    abstract run(): Promise<any>;
    async execute(client: Bot, interaction: ChatInputCommandInteraction): Promise<any> {
        this.interaction = interaction;
        this.permissions = interaction.appPermissions;
        this.caller = interaction.member as GuildMember;
        this.guild = interaction.guild;
        this.client = client;

        let error = await this.checkRequirements();
        if(error.value){ return await interaction.reply({
                embeds: [new ErrorEmbedBuilder(error.message)]
            }); 
        }

        await this.run();

        error = this.afterCommandErrorCheck();
        if(error.value){ return await interaction.reply({
                embeds: [new ErrorEmbedBuilder(error.message)]
            }); 
        }
        await this.sendSuccessMessage()
    };
    autocomplete(client: any, interaction: any): Promise<void> | void { };

    async sendSuccessMessage(): Promise<InteractionResponse<boolean> | Message<boolean>>{
        if(this.successMessage){
            if(this.interaction.replied || this.interaction.deferred){
                return await this.interaction.editReply({ 
                    embeds: [new SuccessEmbedBuilder(this.successMessage)] 
                });
            }
            return await this.interaction.reply({ 
                embeds: [new SuccessEmbedBuilder(this.successMessage)] 
            });
        }
    }
    toJSON(){
        return {
            name: this.name,
            description: this.description,
            options: this.options,
            execute: this.execute,
            errorCheck: this.errorCheck,
            successMessage: this.successMessage,
            run: this.run,
            checkRequirements: this.checkRequirements,
            afterCommandErrorCheck: this.afterCommandErrorCheck,
            sendSuccessMessage: this.sendSuccessMessage,
            autocomplete: this.autocomplete,
        }
    }
};

export abstract class IVoiceCommand extends ICommand{
    errorCheck(): ClientError | Promise<ClientError>{
        if(!this.permissions.has(PermissionsBitField.Flags.Connect)){
            return ClientError.CONNECTPERMISSION;
        }
        if(!this.permissions.has(PermissionsBitField.Flags.Speak)){
            return ClientError.SPEAKPERMISSION;
        }
        if(!this.caller.voice.channel){
            return ClientError.CALLERVOICECHANNEL;
        }
        return this.additionalErrors();
    }
    abstract additionalErrors(): ClientError | Promise<ClientError>;

    clientVoiceStateOnGuild(){
        return this.guild.voiceStates.cache.get(this.client.user.id);
    }

    toJSON(){
        return {
            ...super.toJSON(),
            clientVoiceStateOnGuild: this.clientVoiceStateOnGuild,
            additionalErrors: this.additionalErrors
        };
    }
}

export abstract class IVoiceCommandVoiceConnection extends IVoiceCommand{
    getCallerVoiceChannelID(){
        return this.caller.voice.channelId;
    }
    additionalErrors(): ClientError | Promise<ClientError>{
        if(this.clientVoiceStateOnGuild() === undefined){
            return ClientError.NOTCONNECTED;
        }
        if(this.getCallerVoiceChannelID() !== this.clientVoiceStateOnGuild().channelId){
            return ClientError.NOTTHESAMECHANNEL;
        }
        return this.commandErrors();
    }

    abstract commandErrors(): ClientError | Promise<ClientError>

    toJSON(){
        return {
            ...super.toJSON(),
            commandErrors: this.commandErrors,
            getCallerVoiceChannelID: this.getCallerVoiceChannelID
        }
    }
}