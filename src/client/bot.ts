import { ICommand } from '@/interfaces/command';
import { IEvent } from '@/interfaces/event';
import DatabaseManager from '../utils/manager';
import { Client, Collection, IntentsBitField, REST, Routes } from 'discord.js';
import { glob } from 'glob';
import { join } from 'path';


export default class Bot extends Client{
    public readonly commands = new Collection<string, ICommand>();
    public audioPlayers = {}
    public databaseManager = new DatabaseManager();
    constructor(){
        super({ 
            'intents': [
                IntentsBitField.Flags.Guilds,
                IntentsBitField.Flags.GuildMembers,
                IntentsBitField.Flags.GuildVoiceStates
            ]
        });
        this.init();
    }

    private async init(): Promise<void> {
        this.loadCommands();
        this.loadEvents();
        await this.login(process.env.DISCORD_BOT_TOKEN).then(()=>{
            this.postCommands();
        });
    }

    private postCommands(): void {
        const rest = new REST({ 'version': '10' }).setToken(process.env.DISCORD_BOT_TOKEN);
        console.info('Started loading application (/) commands...');
        rest.put(Routes.applicationCommands(this.user.id), {
            body: this.commands.toJSON()
        })
        .then(() => console.log(`Successfully loaded [${this.commands.size}] application (/) commands.`))
        .catch((err: any) => {
            console.error(err);
            process.exit(1);
        });
    }

    private async loadCommands(): Promise<void> {
        const files = await glob('**/*.ts', { cwd: join(__dirname, '../commands') })
        files.forEach(async file => {
            import(`../commands/${file}`).then((commandJSON)=>{
                const command: ICommand = commandJSON[Object.keys(commandJSON)[0]];
                if (this.commands.get(command.name)) console.error(`Repeated command name. (name: ${command.name}, file: ${file})`);
                else this.commands.set(command.name, command);
            });            
        });
    }

    private async loadEvents(): Promise<void> {
        const files = await glob('**/*.ts', { cwd: join(__dirname, '../events') })
        files.forEach(async file => {
            import(`../events/${file}`).then((eventJSON)=>{
                const event: IEvent = eventJSON[Object.keys(eventJSON)[0]];
                this[event.type](event.name, event.execute.bind(null, this));
            });            
        });
    }
}