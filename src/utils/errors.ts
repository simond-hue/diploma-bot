export class ClientError{
    static readonly DEFAULT = new ClientError(null, null, false)
    static readonly CONNECTPERMISSION = new ClientError('ConnectionPermissionError', 'I have no permission to join the requested voice channel!');
    static readonly SPEAKPERMISSION = new ClientError('SpeakPermissionError', 'I have no permission to speak in the requested voice channels!');
    static readonly CALLERVOICECHANNEL = new ClientError('NoVoiceChannel', 'You are not in a voice channel!');
    static readonly ALREADYCONNECTED = new ClientError('AlreadyConnected', 'I am already on a voice channel!');
    static readonly COULDNOTJOIN = new ClientError('CouldNotJoin', 'The voice connection was not estabilished!');
    static readonly NOTCONNECTED = new ClientError('NotConnected', 'I am not on any voice channel!');
    static readonly NOTTHESAMECHANNEL = new ClientError('NotTheSameChannel', 'We are not on the same voice channel!');
    static readonly INCORRECTLINK = new ClientError('IncorrectLink', 'The given link was incorrect!');
    static readonly ALREADYPAUSED = new ClientError('AlreadyPaused', 'The player is already paused!');
    static readonly EMPTYPLAYLIST = new ClientError('EmptyPlaylist', 'The playlist is empty!');
    static readonly PAUSEERROR = new ClientError('PauseError', 'Pausing was not successful!');
    static readonly ALREADYUNPAUSED = new ClientError('UnpauseError', 'The player is already playing!');
    static readonly UNPAUSEERROR = new ClientError('UnpauseError', 'Resuming was not successful!');
    static readonly USELOOPINSTEAD = new ClientError('UseLoopInstead', 'The playlist only consist one audio source. Consider using loop instead!');
    static readonly NOTAUDIOTYPE = new ClientError('NotAudioType', 'The given attachment is not a playable file!');
    static readonly FILETOOLARGE = new ClientError('FileTooLarge', 'The given file size is too large!');
    static readonly NOVIDEOFOUND = new ClientError('NoVideoFound', 'No video was found with this keyword!');
    static readonly BADINDEX = new ClientError('BadIndex', 'The requested index was incorrect!');
    static readonly USESKIPINSTEAD = new ClientError('UseSkipInstead', 'Use skip instead of remove!');
    static readonly PLAYLISTEXISTS = new ClientError('PlaylistExists', 'A playlist exists with the same name on this server!');
    static readonly TOOMANYPLAYLISTS = new ClientError('TooManyPlaylists', 'There are too many playlists stored on this server already! Delete one in order to save the current one!');
    static readonly NOPERMISSIONTODELETE = new ClientError('NoPermissionToDelete', 'You have no permission to delete this playlist!');
    static readonly NOSUCHPLAYLIST = new ClientError('NoSuchPlaylist', 'There is no playlist stored on this server with this name!');
    static readonly NOPLAYLISTS = new ClientError('NoPlaylists', 'There are no playlists saved on this server!');
    private constructor(readonly name: string, readonly message: string, readonly value?: boolean) {
        if(value === undefined){ this.value = true; }
    }  
}
