const fetch = require("node-fetch");

export default class YouTubeAPI{
    private static readonly playlistEndpoint = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet';
    private static readonly searchEndpoint = 'https://www.googleapis.com/youtube/v3/search?part=id'
    static async getPlaylist(id: string, pageToken: string = null): Promise<string[]> {
        let maxResults = 50;
        let tokenString = '';
        if(pageToken !== null){
            tokenString = `&pageToken=${pageToken}`;
        }
        let playlist: string[] = [];
        const response = await fetch(`${this.playlistEndpoint}&maxResults=${maxResults}&playlistId=${id}&key=${process.env.YOUTUBE_API_KEY}${tokenString}`);
        const data = await response.json(); 
        data.items.forEach((item:any) => {
            let link = `https://www.youtube.com/watch?v=${item.snippet.resourceId.videoId}`;
            playlist.push(link); 
        });

        if(data.hasOwnProperty("nextPageToken")){
            return playlist.concat(await this.getPlaylist(id, data.nextPageToken));
        }
        return playlist;
    }

    static async search(string: string, results: number = 10): Promise<string[]>{
        let foundElements: string[] = [];
        const response = await fetch(`${this.searchEndpoint}&q=${string.split(' ').join('+')}&type=video&key=${process.env.YOUTUBE_API_KEY}&maxResults=${results}`);
        const data = await response.json();
        data.items.forEach((item: any) => {
            let link = `https://www.youtube.com/watch?v=${item.id.videoId}`;
            foundElements.push(link);
        });
        return foundElements;
    }
}