import { Attachment } from "discord.js";
import { FileSong, Song } from "../audio/song";
import { writeFile, readFileSync, writeFileSync } from "fs";

export default class DatabaseManager{
    public static readonly maxEntries = 10;
    private database: any;
    constructor(private path: string = 'database/playlists.json'){
        this.readDB();
    }

    private readDB(): void {
        const data = readFileSync(this.path)
        this.database = JSON.parse(data.toString());
    }

    private writeDB(): void {
        writeFileSync(this.path, JSON.stringify(this.database,null,2))
    }

    getEntryCount(guildID: string): number {
        const currentServer = this.database[guildID];
        return currentServer === undefined ? 0 : Object.keys(this.database[guildID]).length;
    }

    getPlaylistsOn(guildID: string) {
        return this.database[guildID];
    }

    savePlaylist(name: string, guildID: string, data: { playlist: Song[], userID: string, creatorName: string }): void {
        let linkArray: {link: string, fromFile: boolean, attachment?: Attachment }[] = [];
        data.playlist.forEach((item: Song) => {
            linkArray.push({link: item.link, fromFile: item.fromFile, attachment: item.fromFile ? (item as FileSong).attachment : null });
        });
        if(!this.database[guildID]){
            this.database[guildID] = {};
        }
        this.database[guildID][name] = { 'playlist': linkArray, 'createdBy': data.userID, 'creatorName': data.creatorName };
        this.writeDB();
    }

    checkEntry(guildID:string, entryName: string): boolean {
        if(this.database[guildID] && this.database[guildID][entryName]){
            return true;
        }
        return false
    }

    checkServer(guildID: string): boolean {
        return this.database[guildID] ? true : false;
    }

    deletePlaylist(guildID: string, playlistName: string){
        delete this.database[guildID][playlistName];
        this.writeDB();
    }
}