import { Song } from "../audio/song";
import { EmbedBuilder } from "discord.js";
import { formatSecondsToMinutes } from "./utility";

class BaseEmbedBuilder extends EmbedBuilder{
    constructor(protected emoji: string){ 
        super();
    }
}

export class ErrorEmbedBuilder extends BaseEmbedBuilder{
    constructor(private message: string){
        super(':x:');
        this.setColor('Red')
        this.setDescription(`${this.emoji} ${this.message}`)
    }
}

export class SuccessEmbedBuilder extends BaseEmbedBuilder{
    constructor(private message: string){
        super(':white_check_mark:')
        this.setColor('Green');
        this.setDescription(`${this.emoji} ${this.message}`)
    }
}

export class UnpauseEmbedBuilder extends BaseEmbedBuilder{
    constructor(private message: string){
        super(':arrow_forward:')
        this.setColor('Blue');
        this.setDescription(`${this.emoji} ${this.message}`)
    }
}

export class PauseEmbedBuilder extends BaseEmbedBuilder{
    constructor(private message: string){
        super(':pause_button:')
        this.setColor('Blue');
        this.setDescription(`${this.emoji} ${this.message}`)
    }
}

export class NowPlayingEmbedBuilder extends BaseEmbedBuilder{
    constructor(private song: Song, private currentTime: number){
        super(':arrow_forward:');
        this.setColor('Blue');
        this.setTitle(this.emoji + ' Currently playing')
        this.setURL(song.link);
        this.setDescription(song.title);
        this.setThumbnail(song.thumbnail);
        this.setFooter(
            { text: `Kérte: ${song.requestedName}`, iconURL: song.requestedAvatar }
        )
        if(!this.song.isLive){
            this.addFields(
                { name: this.createDisplay(), value: `${formatSecondsToMinutes(this.currentTime)}/${formatSecondsToMinutes(song.length)}` }
            )
        }
        
    }

    private createDisplay(): string {
        var display = "";
        for(var i = 0; i < 50; i++){
            display += "-";
        }
        var index = this.currentTime/this.song.length*100/2;
        display = display.substr(0,index) + "⬤" + display.substr(index+1,display.length);
        return display;
    }
}

export class PlaylistEmbedBuilder extends BaseEmbedBuilder{
    constructor(playlists: any){
        super('');
        this.setColor('Blue');
        this.setTitle('Playlists on this server');
        Object.keys(playlists).forEach(key =>{
            this.addFields({name: key, value: `Playlist length: ${playlists[key].playlist.length}\nCreated by: ${playlists[key].creatorName}` })
        });
    }
}

export class ResultsEmbedBuilder extends BaseEmbedBuilder{
    constructor(songs: Song[]){
        super(':mag:');
        this.setColor('Blue');
        this.setTitle('Found videos');
        songs.forEach((item: Song) =>{
            let index = songs.indexOf(item);
            this.addFields({name: `${index+1}. ${item.title}`, value: `Song length: ${formatSecondsToMinutes(item.length)}`});
        });
    }
}

export class QueueEmbedBuilder extends BaseEmbedBuilder{
    constructor(playlistElements: [{ index: number, song: Song }], page: number, maxPage: number){
        super('');
        this.setColor('Blue');
        this.setTitle(`Page ${page+1}/${maxPage+1}`);
        playlistElements.forEach(element => {
            this.addFields({name: `${element.index+1}. ${element.song.title}`, value: `Song length: ${formatSecondsToMinutes(element.song.length)}`});
        });
    }
}

export const commandErrorEmbed = new ErrorEmbedBuilder('The command could not be run!');