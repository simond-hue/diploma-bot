export function formatSecondsToMinutes(timeInSeconds: number): string{
    const minutes = Math.floor(timeInSeconds / 60);
    const seconds = Math.floor(timeInSeconds - minutes * 60);
    let minutesString = `${minutes}`;
    if(minutes < 10){
        minutesString = `0${minutes}`;
    }
    let secondsString = `${seconds}`;
    if(seconds < 10){
        secondsString = `0${secondsString}`;
    }
    return minutesString.concat(":",secondsString);
}

export function randomBetween(min: number, max: number): number{
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const channels = 2;

export function getFileLengthInSeconds(size: number, bitrate: number){
    return size/(bitrate/8)/channels;
}
