import { randomBetween } from "../utils/utility";
import { AudioPlayer, AudioPlayerStatus, createAudioResource } from "@discordjs/voice";
import { FileSong, Song, YoutubeSong } from "./song";
import internal from "stream";
const fetch = require("node-fetch");

export class AutomatedAudioPlayer extends AudioPlayer{
    playlist: Song[] = [];
    paused = false;
    currentIndex: number = 0
    time: number = 0;
    looped: boolean = false;
    shuffled: boolean = false;
    private shuffleMode: boolean = false;
    private timer: any;
    private page: number = 0;
    private itemPerPage: number = 5;
    constructor(){ 
        super();
        this.init();
    }

    init(): void {
        this.on(AudioPlayerStatus.Idle, () => {
            this.time = 0; 
            clearInterval(this.timer);
            this.playNext();
        });
        this.on(AudioPlayerStatus.Playing, () => {
            this.timer = setInterval(()=>{ 
                if(!this.paused){
                    this.time += 1 
                }
            }, 1000)
        })
        this.on('error', (error)=>{
            console.log(error);
            if(this.playlist.length > 0){
                this.skip();
            }
        })
    }

    pause(): boolean {
        this.paused = true;
        return super.pause();
    }

    unpause(): boolean {
        this.paused = false;
        return super.unpause();   
    }

    remove(index: number): void {
        this.playlist.splice(index, 1);
    }

    addToPlaylist(item: Song): void {
        this.playlist.push(item);
        if(this.playlist.length === 1){
            this.playAudio(this.playlist[this.currentIndex]);
        }
    }

    toggleLoop(){
        this.looped = !this.looped;
        return this.looped;
    }

    toggleShuffle(){
        this.shuffled = !this.shuffled;
        return this.shuffled;
    }

    playNext(): void {
        if(!this.looped && !(this.shuffled || this.shuffleMode)){
            this.playlist.shift();
        }
        this.shuffleMode = this.shuffled;
        if(this.shuffled && !this.looped){
            this.currentIndex = randomBetween(0,this.playlist.length-1);
        }
        if(!this.shuffled && !this.looped){
            this.currentIndex = 0;
        }
        if(this.playlist.length > 0){
            this.playAudio(this.playlist[this.currentIndex]);
        }
    }

    clearPlaylist(){
        this.playlist.splice(1,this.playlist.length);
    }

    skip(): void {
        this.looped = false;
        this.stop();
    }

    async playAudio(item: Song): Promise<void> {
        if(!item.fromFile){
            let youtubeSong = item as YoutubeSong;
            if(!youtubeSong.info){
                await youtubeSong.getInfo();
            }
            try{
                this.play(createAudioResource(youtubeSong.downloadFromInfo()));
            }
            catch(err){
                this.emit(AudioPlayerStatus.Idle);
                console.log(err);
            }
        }
        if(item.fromFile){
            const stream = (await fetch((item as FileSong).getStreamURL())).body;
            const resource = createAudioResource(stream as unknown as internal.Readable);
            this.play(resource);
        }
    }

    getCurrentSong(): Song {
        return this.playlist[this.currentIndex];
    }

    isPlaylistEmpty(): boolean {
        return this.playlist.length === 0;
    }

    getPageOfSongs() {
        let playlistELements: any[] = [];
        let index = this.page * this.itemPerPage
        let songs = this.playlist.slice(index, index+this.itemPerPage);
        songs.forEach((value: Song) =>{
            playlistELements.push({
                index: index,
                song: value
            })
            index++
        })
        return playlistELements;
    }

    addToPage(){
        let maxPageCount = this.getMaxPage();
        if(this.page < maxPageCount - 1){
            this.page++;
        }
    }

    subtractFromPage(){
        if(this.page > 0){
            this.page--;
        }
    }

    getPage(){
        return this.page;
    }

    getMaxPage(){
        return Math.floor(this.playlist.length / this.itemPerPage);
    }

    setPageToDefault(){
        this.page = Math.floor(this.currentIndex / this.itemPerPage);
    }

    isPageMaxPage(){
        return this.page === this.getMaxPage() - 1;
    }

    isPageFirstPage(){
        return this.page === 0;
    }
}
