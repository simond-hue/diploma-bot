import { getFileLengthInSeconds } from "../utils/utility";
import { Attachment, GuildMember } from "discord.js";
import { Readable } from "stream";
import ytdl from "ytdl-core";

export class Song{
    title: string;
    fromFile: boolean;
    error: boolean = false;
    requestedAvatar: string;
    requestedName: string;
    length: number;
    thumbnail: string;
    isLive: boolean;
    link: string;

    constructor(requestedBy: GuildMember){
        this.requestedAvatar = requestedBy.displayAvatarURL();
        this.requestedName = requestedBy.displayName;
    }
}

export class FileSong extends Song{
    fromFile: boolean = true;
    isLive: boolean = false;
    thumbnail: string = 'attachment://file.png';
    constructor(public attachment: Attachment, requestedBy: GuildMember){
        super(requestedBy);
        this.title = attachment.name.split('_').join(" ");
        this.link = this.attachment.url;
        this.length = getFileLengthInSeconds(this.attachment.size, requestedBy.voice.channel.bitrate);
    }

    getStreamURL(){
        return this.link;
    }
}

export class YoutubeSong extends Song{
    info: any;
    formats: [];
    fromFile: boolean = false;
    constructor(public link: string, requestedBy: GuildMember){
        super(requestedBy);
        this.requestedAvatar = requestedBy.displayAvatarURL();
        this.requestedName = requestedBy.displayName;
    }

    async getInfo(): Promise<void> {
        this.info = await ytdl.getInfo(this.link, {
            requestOptions: {
                headers: {
                    cookie: process.env.COOKIE,
                    "x-youtube-identity-token": process.env.X_YOUTUBE_IDENTIFY_TOKEN
                }
            }
        }).catch(err => console.log(err));
        if(this.info === undefined){
            return;
        }
        this.title = this.info.videoDetails.title;
        this.thumbnail = this.info.player_response.videoDetails.thumbnail.thumbnails[0].url;
        this.isLive = this.info.player_response.videoDetails.isLive;
        this.formats = this.info.formats;
        this.length = this.info.videoDetails.lengthSeconds;
    }

    downloadFromInfo(): Readable {
        return ytdl.downloadFromInfo(this.info, {filter: 'audioonly',quality:'highestaudio',highWaterMark:1<<25});
    }
}
